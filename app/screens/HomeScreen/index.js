import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import Image from "components/Image";
import ProgressCard from "./ProgressCard";
import DocumentCard from "./DocumentCard";
import EarningCard from "./EarningCard";

import style from "./style";

function HomeScreen({ navigation }) {
  return (
    <View style={style.container}>
      <Image title="logo" style={style.logo} />
      <ProgressCard steps={3} navigation={navigation} />
      <EarningCard />
      <DocumentCard navigation={navigation} />
    </View>
  );
}
HomeScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(HomeScreen);
