import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import PropTypes from "prop-types";

import style from "./style";

function DocumentCard({ navigation }) {
  return (
    <View style={style.container}>
      <TouchableOpacity onPress={() => navigation.navigate("directorDetail")}>
        <Text style={style.heading}>UPLOAD DIRECTOR DETAILS</Text>
      </TouchableOpacity>
      <View style={style.iconHolder}>
        <Icon name="upload-cloud" size={25} color="black" />
      </View>
      <Text style={style.heading}>UPLOAD SHAREHOLDER DETAILS</Text>
    </View>
  );
}
DocumentCard.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(DocumentCard);
