import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: Colors.white,
    padding: Dimensions.space3x,
    // paddingHorizontal: Dimensions.space3x,
    borderRadius: Dimensions.radius3x,
    marginTop: Dimensions.space3x,
    justifyContent: "space-between",
    alignItems: "center",
    ...elevation(3),
  },
  iconHolder: {
    width: 50,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 25,
    borderColor: Colors.primary1Color,
    borderWidth: 1,
  },
  iconStyle: {
    fontSize: 22,
  },
  heading: {
    width: 100,
    fontSize: 14,
    fontWeight: "500",
    color: Colors.textBlack,
    textAlign: "auto",
  },
});

export default style;
