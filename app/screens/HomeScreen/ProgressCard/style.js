import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  CardHolder: {
    backgroundColor: Colors.white,
    padding: Dimensions.space3x,
    // paddingHorizontal: Dimensions.space3x,
    borderRadius: Dimensions.radius3x,
    marginTop: Dimensions.space3x,
    ...elevation(3),
  },
  progressBarContainer: {
    width: "100%",
  },
  progressHeadingHolder: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  mainHeading: {
    fontSize: 20,
    fontWeight: "200",
    color: Colors.textBlack,
  },
  subHeading: {
    fontSize: 18,
    fontWeight: "900",
    color: Colors.textBlack,
  },
  percentText: {
    fontSize: 18,
    fontWeight: "500",
    color: Colors.corporate1Color,
  },
  progressBarHolder: {
    width: "100%",
    backgroundColor: Colors.primary1Color,
    borderRadius: Dimensions.radius1x,
    marginHorizontal: 1,
    height: 5,
    marginTop: Dimensions.space1x,
  },
  progress: {
    backgroundColor: Colors.corporate1Color,
    borderRadius: Dimensions.radius1x,
    height: 5,
  },
});

export default style;
