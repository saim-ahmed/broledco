import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    padding: Dimensions.space2x,
  },
  logo: {
    width: Dimensions.screenWidth - 100,
    height: 80,
    alignSelf: "center",
  },
});

export default style;
