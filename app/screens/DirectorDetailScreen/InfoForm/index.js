import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import { Formik } from "formik";

import FlatButton from "theme/flatButton";
import Input from "theme/input";

import { isValidNumber, isValidEmail } from "utils/validations";

import Header from "./Header";
import style from "./style";

const initialValues = {
  fullName: "",
  phone: "",
  email: "",
  address: "",
};

const inputRefs = [];
const INPUT_FIELDS = [
  {
    key: "fullName",
    keyboardType: "default",
    placeholder: "Full Name",
    error: "invalid name",
  },
  {
    key: "phone",
    keyboardType: "numeric",
    placeholder: "Phone",
    error: "invalid name",
  },
  {
    key: "email",
    keyboardType: "email-address",
    placeholder: "Email",
    error: "invalid email",
  },
  {
    key: "address",
    keyboardType: "default",
    placeholder: "Address",
    error: "please fill address",
  },
];

function InfoForm({ visible, onRequestClose }) {
  const focusInput = index => {
    try {
      inputRefs[index].focus();
    } catch (e) {
      console.log(e);
    }
  };
  const validateFiled = values => {
    const errors = {};
    if (values.fullName === "") {
      errors.fullName = true;
    }
    if (values.phone === "" || !isValidNumber(values.phone)) {
      errors.phone = true;
    }
    if (values.email === "" || !isValidEmail(values.email)) {
      errors.email = true;
    }
    if (values.address === "") {
      errors.address = true;
    }
    return errors;
  };

  const renderField = (field, formikProps) => {
    const { key } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.inputWrapper} key={key}>
        <Input
          innerRef={node => {
            inputRefs[field.index] = node;
          }}
          keyboardType={field.keyboardType}
          value={value}
          onChangeText={e => formikProps.setFieldValue(`${key}`, e)}
          placeholder={field.placeholder}
          onBlur={() => {
            formikProps.setFieldTouched(`${key}`);
          }}
          error={
            formikProps.touched[key] && formikProps.errors[key]
              ? field.error
              : null
          }
          isValid={!!(formikProps.touched[key] && !formikProps.errors[key])}
          onSubmitEditing={() => focusInput(field.index + 1)}
          {...field.inputProps}
        />
      </View>
    );
  };
  return visible ? (
    <Animatable.View
      useNativeDriver
      animation="bounceIn"
      easing="ease-in-out"
      duration={200}
      style={style.modal}
    >
      <Header
        title="Fill Director Information"
        onRequestClose={onRequestClose}
      />
      <Formik
        key="form"
        initialValues={initialValues}
        onSubmit={values => console.log("values", values)}
        validate={validateFiled}
        render={formikProps => (
          <React.Fragment>
            {INPUT_FIELDS.map((field, index) =>
              renderField({ ...field, index }, formikProps)
            )}
            <View style={style.loginButtonHolder}>
              <FlatButton
                onPress={() => console.log("Sign In")}
                label="submit"
                secondary
                round
                disable={
                  Object.keys(formikProps.errors).length !== 0 ||
                  Object.keys(formikProps.touched).length < 1
                }
              />
            </View>
          </React.Fragment>
        )}
      />
    </Animatable.View>
  ) : null;
}
InfoForm.propTypes = {
  //   navigation: PropTypes.func.isRequired,
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
};

InfoForm.defaultProps = {
  visible: false,
};

export default React.memo(InfoForm);
