import { StyleSheet } from "react-native";

import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  modal: {
    flexDirection: "column",
    backgroundColor: Colors.white,
    width: Dimensions.screenWidth,
    paddingBottom: Dimensions.bottomSpacing,
    borderTopRightRadius: Dimensions.space4x,
    borderTopLeftRadius: Dimensions.space4x,
    ...elevation(1),
    position: "absolute",
    bottom: 0,
    left: 0,
    zIndex: 11,
  },
  headerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: Colors.primary2Color,
    height: 44,
  },
  headerHeading: {
    fontSize: 20,
    fontWeight: "500",
    color: Colors.textBlack,
    marginLeft: Dimensions.space2x,
  },
  closeIconHolder: {
    marginRight: Dimensions.space1x,
    height: 35,
    width: 35,
    justifyContent: "center",
    alignItems: "center",

    borderRadius: 22.5,
    borderWidth: 1,
    borderColor: Colors.primary2Color,
    // ...elevation(1),
  },
  closeIcon: {
    fontSize: 22,
    fontWeight: "500",
    color: Colors.textBlack,
  },
  loginButtonHolder: {
    padding: Dimensions.space3x,
  },
});

export default style;
