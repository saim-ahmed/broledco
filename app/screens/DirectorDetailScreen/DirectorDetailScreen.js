import React, { useState } from "react";
import { Text, View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Header from "components/Header";
import ListItem from "components/ListItem";

import DirectorCountCard from "./DirectorCountCard";
import InfoForm from "./InfoForm";
import DocForm from "./DocForm";
import style from "./style";

function DirectorDetailScreen({ navigation }) {
  const [activeTab, setActiveTab] = useState(0);
  const [directorCount, setDirectorCount] = useState(1);
  const [infoForm, setInfoForm] = useState(false);
  const [docForm, setDocForm] = useState(false);
  return (
    <View>
      <Header navigation={navigation} title="Director Detail" />
      <View style={[style.container]}>
        <View style={style.countCardHolder}>
          <DirectorCountCard
            directorCount={directorCount}
            onPressUp={() => setDirectorCount(directorCount + 1)}
            onPressDown={() => {
              if (directorCount === 1) {
                setDirectorCount(1);
                return;
              }
              setDirectorCount(directorCount - 1);
            }}
          />
          <View style={style.percentCard} />
        </View>
        <View style={style.tabHolder}>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(0)}>
            <Text
              style={[
                style.informationLabel,
                activeTab === 0 ? style.activeTab : null,
              ]}
            >
              Fill Director Information
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.tab} onPress={() => setActiveTab(1)}>
            <Text
              style={[
                style.documentLabel,
                activeTab === 1 ? style.activeTab : null,
              ]}
            >
              Upload Director Documents
            </Text>
          </TouchableOpacity>
        </View>

        <ListItem
          type={activeTab === 0 ? "Information" : "Document"}
          items={directorCount}
          onPress={() => {
            if (activeTab === 0) {
              setInfoForm(true);
              return;
            }
            setDocForm(true);
          }}
        />
      </View>
      {infoForm || docForm ? <View style={style.overlay} /> : null}
      <InfoForm onRequestClose={() => setInfoForm(false)} visible={infoForm} />
      <DocForm onRequestClose={() => setDocForm(false)} visible={docForm} />
    </View>
  );
}
DirectorDetailScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(DirectorDetailScreen);
