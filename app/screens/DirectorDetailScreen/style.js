import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
// import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    // padding: Dimensions.space2x,
  },
  countCardHolder: {
    flexDirection: "row",
    padding: Dimensions.space2x,
    backgroundColor: Colors.corporate1Color,
  },
  percentCard: {
    width: 100,
    height: 50,
  },
  tabHolder: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: Colors.primary2Color,
    padding: Dimensions.space2x,
    marginVertical: Dimensions.space2x,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  tab: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center",
    height: 50,
  },
  informationLabel: {
    fontSize: 16,
    fontWeight: "500",
    color: Colors.corporate3Color,
  },
  documentLabel: {
    fontSize: 16,
    fontWeight: "500",
    color: Colors.corporate3Color,
    marginLeft: Dimensions.space3x,
  },
  activeTab: {
    borderBottomWidth: 3,
    borderColor: Colors.corporate1Color,
    paddingBottom: Dimensions.space2x,
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    height: Dimensions.screenHeight,
    width: Dimensions.screenWidth,
    backgroundColor: Colors.translucentBlack,
    opacity: 0.8,
    zIndex: 10,
  },
});

export default style;
