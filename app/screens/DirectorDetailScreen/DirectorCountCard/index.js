import React from "react";
import { View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import Text from "components/Text";

import style from "./style";

function DirectorCountCard({
  directorCount,
  onPressUp,
  onPressDown,
  navigation,
}) {
  return (
    <View style={style.container}>
      <Text style={style.heading}>Number Of Directors</Text>
      <View style={style.countHolder}>
        <TouchableOpacity onPress={onPressUp}>
          <Icon name="chevron-up" size={30} color="black" />
        </TouchableOpacity>
        <Text style={style.countLabel}>{directorCount}</Text>
        <TouchableOpacity onPress={onPressDown}>
          <Icon name="chevron-down" size={30} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
}
DirectorCountCard.propTypes = {
  navigation: PropTypes.func.isRequired,
  onPressUp: PropTypes.func.isRequired,
  onPressDown: PropTypes.func.isRequired,
  directorCount: PropTypes.number.isRequired,
};

export default React.memo(DirectorCountCard);
