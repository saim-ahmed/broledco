import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    backgroundColor: Colors.primary1Color,
    borderRadius: Dimensions.radius3x,
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: Dimensions.space1x,
    ...elevation(2),
  },
  heading: {
    fontSize: 20,
    fontWeight: "500",
    color: Colors.textBlack,
    paddingLeft: Dimensions.space1x,
  },
  countHolder: {
    // margin: Dimensions.space2x,
    marginLeft: Dimensions.space1x,
    paddingHorizontal: Dimensions.space1x,
    borderWidth: 1,
    borderColor: Colors.primary2Color,
    backgroundColor: Colors.primary1Color,
    justifyContent: "center",
    alignItems: "center",
  },
  countLabel: {
    fontSize: 20,
    fontWeight: "500",
    color: Colors.textBlack,
  },
});

export default style;
