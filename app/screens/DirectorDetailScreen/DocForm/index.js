import React from "react";
import { View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import { Formik } from "formik";
import Icon from "react-native-vector-icons/Feather";

import FlatButton from "theme/flatButton";
import Text from "components/Text";

import Header from "./Header";
import style from "./style";

const initialValues = {
  doc1: "",
  doc2: "",
};

const INPUT_FIELDS = [
  {
    key: "doc1",
    title: "Upload passport copy or driving license or new NIC",
  },
  {
    key: "doc2",
    title: "Upload billing proof",
  },
];

function DocForm({ visible, onRequestClose }) {
  const renderField = field => {
    const { key } = field;
    return (
      <TouchableOpacity style={style.cardWrapper} key={key}>
        <Icon name="file-text" size={40} />
        <Text style={style.cardTitle}>{field.title}</Text>
      </TouchableOpacity>
    );
  };
  return visible ? (
    <Animatable.View
      useNativeDriver
      animation="bounceIn"
      easing="ease-in-out"
      duration={200}
      style={style.modal}
    >
      <Header title="Add Director Documents" onRequestClose={onRequestClose} />
      <Formik
        key="form"
        initialValues={initialValues}
        onSubmit={values => console.log("values", values)}
        validate={null}
        render={formikProps => (
          <React.Fragment>
            {INPUT_FIELDS.map((field, index) =>
              renderField({ ...field, index }, formikProps)
            )}
            <View style={style.loginButtonHolder}>
              <FlatButton
                onPress={() => console.log("Sign In")}
                label="submit"
                secondary
                round
                disable={
                  Object.keys(formikProps.errors).length !== 0 ||
                  Object.keys(formikProps.touched).length < 1
                }
              />
            </View>
          </React.Fragment>
        )}
      />
    </Animatable.View>
  ) : null;
}
DocForm.propTypes = {
  //   navigation: PropTypes.func.isRequired,
  visible: PropTypes.bool,
  onRequestClose: PropTypes.func.isRequired,
};

DocForm.defaultProps = {
  visible: false,
};

export default React.memo(DocForm);
