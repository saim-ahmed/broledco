import React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity } from "react-native";

import Text from "components/Text";

import style from "./style";

function Header({ onRequestClose, title }) {
  return (
    <View style={style.headerContainer}>
      <Text style={style.headerHeading}>{title}</Text>
      <TouchableOpacity style={style.closeIconHolder} onPress={onRequestClose}>
        <Text style={style.closeIcon}>X</Text>
      </TouchableOpacity>
    </View>
  );
}

Header.propTypes = {
  onRequestClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

//   Header.defaultProps = {
//     visible: false,
//   };

export default React.memo(Header);
