import React, { useState } from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import { TabView } from "react-native-tab-view";
import HomeScreen from "screens/HomeScreen/loadable";
import PaymentScreen from "screens/PaymentScreen/loadable";
import ContactScreen from "screens/ContactScreen/loadable";
import MoreTabScreen from "screens/MoreTabScreen/loadable";
import TabBarButton from "./TabBarButton";
import style from "./style";

function MainScreen({ navigation }) {
  const [index, setIndex] = useState(0);
  // eslint-disable-next-line
  const [routes, setRoutes] = useState([
    { key: "Home", icon: "home" },
    { key: "Payment", icon: "shopping-cart" },
    { key: "Contact", icon: "message-square" },
    { key: "More", icon: "more-horizontal" },
  ]);
  // eslint-disable-next-line
  const renderScene = ({ route }) => {
    switch (route.key) {
      case "Home":
        return <HomeScreen navigation={navigation} />;
      case "Payment":
        return <PaymentScreen navigation={navigation} />;
      case "Contact":
        return <ContactScreen navigation={navigation} />;
      case "More":
        return <MoreTabScreen navigation={navigation} />;
      default:
        return null;
    }
  };
  // eslint-disable-next-line
  const renderTabBar = ({ navigationState, jumpTo }) => (
    <View style={style.tabBar}>
      <View style={style.tabBarBackground} />
      {navigationState.routes.map((route, i) => (
        <TabBarButton
          route={route}
          key={route.key}
          onPress={() => {
            jumpTo(route.key);
          }}
          active={i === navigationState.index}
        />
      ))}
    </View>
  );

  return (
    <TabView
      navigationState={{ index, routes }}
      renderScene={renderScene}
      renderTabBar={renderTabBar}
      onIndexChange={i => setIndex(i)}
      tabBarPosition="bottom"
    />
  );
}
MainScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default React.memo(MainScreen);
