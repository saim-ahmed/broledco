import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Animated, TouchableWithoutFeedback, Text } from "react-native";
import Colors from "theme/Colors";
import Icon from "react-native-vector-icons/Feather";
import styles from "./style";

const AnimatedIcon = Animated.createAnimatedComponent(Icon);
const AnimatedText = Animated.createAnimatedComponent(Text);

function TabBarButton({ route, active, onPress }) {
  const animatedValue = new Animated.Value(active ? 1 : 0);

  useEffect(() => {
    Animated.timing(animatedValue, {
      toValue: active ? 1 : 0,
      duration: 200,
    }).start();
  }, [active]);

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Animated.View
        style={[
          styles.tabBarButton,
          {
            borderColor: animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [Colors.transparent, Colors.corporate3Color],
            }),
            backgroundColor: animatedValue.interpolate({
              inputRange: [0, 1],
              outputRange: [Colors.transparent, Colors.primary1Color],
            }),
          },
        ]}
      >
        <AnimatedIcon
          name={route.icon}
          style={[
            styles.tabBarButtonIcon,
            {
              color: animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [Colors.textBlack, Colors.corporate1Color],
              }),
            },
          ]}
        />
        <AnimatedText
          style={[
            styles.tabBarButtonText,
            {
              color: animatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [Colors.textBlack, Colors.corporate1Color],
              }),
            },
          ]}
        >
          {route.key}
        </AnimatedText>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
}

TabBarButton.propTypes = {
  route: PropTypes.shape({
    key: PropTypes.string.isRequired,
    major: PropTypes.bool,
    icon: PropTypes.string.isRequired,
  }).isRequired,
  active: PropTypes.bool,
  onPress: PropTypes.func.isRequired,
};

TabBarButton.defaultProps = {
  active: false,
};

export default TabBarButton;
