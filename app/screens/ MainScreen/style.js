import { StyleSheet, Platform } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";
const TAB_BAR_HEIGHT = 50;
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    width: Dimensions.screenWidth,
    height: Dimensions.screenHeight,
    top: 0,
    left: 0,
    position: "absolute",
  },

  tabBar: {
    width: Dimensions.screenWidth,
    height: TAB_BAR_HEIGHT + Dimensions.space2x,
    flexDirection: "row",
    overflow: "visible",
    position: "relative",
    alignItems: "center",
    justifyContent: "flex-start",
    paddingTop: Dimensions.space2x,
    marginTop: -Dimensions.space2x,
    ...Platform.select({
      ios: {
        ...elevation(2),
        height: TAB_BAR_HEIGHT + Dimensions.space2x + Dimensions.bottomSpacing,
        paddingBottom: Dimensions.bottomSpacing,
      },
      android: {
        ...elevation(8, false),
      },
    }),
  },
  tabBarBackground: {
    width: Dimensions.screenWidth,
    height: TAB_BAR_HEIGHT,
    zIndex: 1,
    flexDirection: "row",
    overflow: "visible",
    backgroundColor: Colors.white,
    borderTopWidth: 1,
    borderTopColor: Colors.primary1Color,
    bottom: 0,
    left: 0,
    position: "absolute",
    flex: 1,
    ...Platform.select({
      ios: {
        ...elevation(2),
        height: TAB_BAR_HEIGHT + Dimensions.bottomSpacing,
        paddingBottom: Dimensions.bottomSpacing,
      },
      android: {},
    }),
  },
  tabBarButton: {
    alignItems: "center",
    justifyContent: "center",
    height: TAB_BAR_HEIGHT,
    paddingHorizontal: Dimensions.space1x,
    flex: 1,
    borderTopWidth: 2,
    zIndex: 3,
  },
  tabBarMajorButtonView: {
    minWidth: 64,
    minHeight: 64,
    maxWidth: 64,
    maxHeight: 64,
    borderRadius: 50,
    backgroundColor: Colors.white,
    borderTopWidth: 0,
    ...elevation(2),
  },
  tabBarButtonImage: {
    width: 48,
    height: 48,
  },
  tabBarButtonText: {
    textAlign: "center",
    color: Colors.textBlack,
    fontSize: 11,

    backgroundColor: Colors.transparent,
  },
  tabBarButtonTextActive: {
    color: Colors.accent1Color,
  },
  tabBarButtonIcon: {
    fontSize: 20,
    fontWeight: "600",
    color: Colors.textBlack,
  },
  indicator: {
    backgroundColor: Colors.accent1Color,
    top: 0,
  },
});

export default style;
