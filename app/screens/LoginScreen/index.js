import React, { useState } from "react";
import { View, Text, ScrollView } from "react-native";
import PropTypes from "prop-types";
import { Formik } from "formik";

import AuthFooter from "components/AuthFooter";

import Header from "components/Header";
import Image from "components/Image";
import FlatButton from "theme/flatButton";
import Input from "theme/input";
import Checkbox from "theme/checkBox";

import style from "./style";

const initialValues = {
  userName: "",
  password: "",
};

const inputRefs = [];
const INPUT_FIELDS = [
  {
    key: "userName",
    keyboardType: "default",
    placeholder: "User Name",
    error: "invalid user name",
  },

  {
    key: "password",
    keyboardType: "default",
    placeholder: "Password",
    error: "please enter password",
    inputProps: {
      secureTextEntry: true,
    },
  },
];

function LoginScreen(props) {
  const [rememberPass, setRememberPass] = useState("");
  const focusInput = index => {
    try {
      inputRefs[index].focus();
    } catch (e) {
      console.log(e);
    }
  };
  const validateFiled = values => {
    const errors = {};
    if (values.userName === "") {
      errors.userName = true;
    }
    if (values.password === "") {
      errors.password = true;
    }
    return errors;
  };

  const renderField = (field, formikProps) => {
    const { key } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.inputWrapper} key={key}>
        <Input
          innerRef={node => {
            inputRefs[field.index] = node;
          }}
          keyboardType={field.keyboardType}
          value={value}
          onChangeText={e => formikProps.setFieldValue(`${key}`, e)}
          placeholder={field.placeholder}
          onBlur={() => {
            formikProps.setFieldTouched(`${key}`);
          }}
          error={
            formikProps.touched[key] && formikProps.errors[key]
              ? field.error
              : null
          }
          isValid={!!(formikProps.touched[key] && !formikProps.errors[key])}
          onSubmitEditing={() => focusInput(field.index + 1)}
          {...field.inputProps}
        />
      </View>
    );
  };
  return (
    <React.Fragment>
      <Header navigation={props.navigation} />
      <View style={style.screen}>
        <ScrollView
          style={style.scrollView}
          contentContainerStyle={style.contentContainer}
          showsVerticalScrollIndicator={false}
        >
          <View style={style.headerContent}>
            <Image style={style.logo} />
            <Text style={style.welcomeHeading}>Welcome</Text>
            <Text style={style.loginHeading}>Login</Text>
          </View>

          <Formik
            key="form"
            initialValues={initialValues}
            onSubmit={values => console.log("values", values)}
            validate={validateFiled}
            render={formikProps => (
              <React.Fragment>
                {INPUT_FIELDS.map((field, index) =>
                  renderField({ ...field, index }, formikProps)
                )}
                <View style={style.userHelpHolder}>
                  <Checkbox
                    active={rememberPass}
                    onPress={() => setRememberPass(true)}
                    label="Remember password"
                  />
                  <Text style={style.forgetPassLabel}> Forget Password</Text>
                </View>
                <View style={style.loginButtonHolder}>
                  <FlatButton
                    onPress={() => console.log("Sign In")}
                    label="Login"
                    secondary
                    round
                    disable={
                      Object.keys(formikProps.errors).length !== 0 ||
                      Object.keys(formikProps.touched).length < 1
                    }
                  />
                </View>
              </React.Fragment>
            )}
          />
        </ScrollView>
      </View>
      <AuthFooter />
    </React.Fragment>
  );
}
LoginScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(LoginScreen);
