import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";

const style = StyleSheet.create({
  screen: {
    flex: 1,
    width: Dimensions.screenWidth,
    height: Dimensions.screenHeight,
    backgroundColor: Colors.white,
  },
  headerContent: {
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
  },
  scrollView: {
    marginTop: Dimensions.space10x,
  },
  contentContainer: {
    paddingTop: Dimensions.space8x,
  },
  logo: {
    width: 80,
    height: 80,
    // marginTop: Dimensions.space2x,
  },
  welcomeHeading: {
    fontSize: 20,
    // fontFamily: "Quicksand",
    fontWeight: "500",
    color: Colors.textBlack,
    marginTop: Dimensions.space2x,
  },
  loginHeading: {
    fontSize: 42,
    fontWeight: "bold",
    // fontFamily: "Quicksand",
    color: Colors.corporate1Color,
    // marginTop: Dimensions.space2x,
  },
  inputHolder: {
    justifyContent: "center",
    alignItems: "flex-start",
    paddingVertical: Dimensions.space2x,
  },
  userHelpHolder: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: Dimensions.space2x,
    justifyContent: "space-between",
  },
  forgetPassLabel: {
    fontSize: 14,
    color: Colors.textBlack,
    marginLeft: Dimensions.space1x,
  },
  loginButtonHolder: {
    padding: Dimensions.space3x,
  },
  facebook: {
    backgroundColor: Colors.facebook,
  },
  google: {
    backgroundColor: Colors.google,
  },
});

export default style;
