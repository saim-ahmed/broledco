import React from "react";
import { View, ScrollView, Text } from "react-native";
import PropTypes from "prop-types";
import { Formik } from "formik";
import Icon from "react-native-vector-icons/Ionicons";
import Header from "components/Header";
import Checkbox from "theme/checkBox";
import FlatButton from "theme/flatButton";

import CHECK_FIELDS from "./checkBoxData";
import style from "./style";

const PAYMENT_METHODS = [
  {
    key: "credit",
    label: "Credit Debit Card",
  },
  {
    key: "payPal",
    label: "PayPal",
  },
  {
    key: "cash",
    label: "Jez Cash / M cash",
  },
  {
    key: "bank",
    label: "Bank Payment",
  },
];

class PaymentScreen extends React.PureComponent {
  constructor() {
    super();
    this.initialValues = {};
    this.paymentMethodInitialValues = {};
    CHECK_FIELDS.forEach(field => {
      this.initialValues[field.key] = false;
    });
    PAYMENT_METHODS.forEach(field => {
      this.paymentMethodInitialValues[field.key] = false;
    });
  }

  renderField = (field, formikProps) => {
    const { key, label } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.checkBoxHolder} key={key}>
        <Checkbox
          active={value}
          onPress={() => formikProps.setFieldValue(`${key}`, !value)}
          label={label}
        />
      </View>
    );
  };

  renderPaymentMethod = (field, formikProps) => {
    const { key, label } = field;
    const value = formikProps.values[key];
    return (
      <View style={style.paymentMethodCheckBoxHolder} key={key}>
        <Checkbox
          active={value}
          onPress={() => formikProps.setFieldValue(`${key}`, !value)}
          label={label}
        />
      </View>
    );
  };

  render() {
    return (
      <React.Fragment>
        <Header navigation={this.props.navigation} title="Order Review" />
        <Formik
          key="form"
          initialValues={this.initialValues}
          render={formikProps => (
            <View style={style.checkBoxWrapper}>
              <ScrollView style={style.scrollView} showsVerticalScrollIndicator>
                {CHECK_FIELDS.map(field =>
                  this.renderField({ ...field }, formikProps)
                )}
              </ScrollView>
            </View>
          )}
        />
        <View style={style.paymentMethodContainer}>
          <View style={style.paymentHeader}>
            <Icon name="md-cash" size={30} color="black" />
          </View>
          <Text style={style.paymentMethodHeading}>Other Payment Methods</Text>
          <Formik
            key="form"
            initialValues={this.paymentMethodInitialValues}
            render={formikProps => (
              <React.Fragment>
                {PAYMENT_METHODS.map(field =>
                  this.renderPaymentMethod({ ...field }, formikProps)
                )}
              </React.Fragment>
            )}
          />
        </View>
        <FlatButton
          onPress={() => console.log("Sign In")}
          label="Pay Now (US$27)"
          secondary
        />
      </React.Fragment>
    );
  }
}
PaymentScreen.propTypes = {
  navigation: PropTypes.func.isRequired,
};

export default React.memo(PaymentScreen);
