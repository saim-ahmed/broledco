import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  scrollView: {
    height: 260,
    // borderWidth: 1,
    // borderColor: Colors.corporate1Color,
    paddingVertical: Dimensions.space2x,
    ...elevation(1),
  },
  checkBoxWrapper: {
    padding: Dimensions.space1x,
  },
  checkBoxHolder: {
    justifyContent: "center",
    padding: Dimensions.space1x,
    margin: Dimensions.space1x,
    borderRadius: Dimensions.radius2x,
    backgroundColor: Colors.primary1Color,
    ...elevation(2),
  },
  paymentMethodContainer: {
    margin: Dimensions.space2x,
    padding: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    ...elevation(1),
  },
  paymentHeader: {
    paddingBottom: Dimensions.space1x,
    borderBottomWidth: 1,
    borderBottomColor: Colors.corporate1Color,
  },
  paymentMethodHeading: {
    fontSize: 18,
    fontWeight: "800",
    color: Colors.textBlack,
  },
  paymentMethodCheckBoxHolder: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.primary1Color,
  },
});

export default style;
