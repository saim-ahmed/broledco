const CHECK_FIELDS = [
  {
    key: "1",
    label: "mobile number verification",
  },
  {
    key: "2",
    label: "reservation of company name",
  },
  {
    key: "3",
    label: "payment for application processing fee-Rs 5000",
  },
  {
    key: "4",
    label: "upload directors document and details",
  },
  {
    key: "5",
    label: "upload shareholders document and details",
  },
  {
    key: "6",
    label: "review of directors and shareholders documents",
  },
  {
    key: "7",
    label:
      "payment after registering on British company registry ( after online verification) Rs 35,000",
  },
  {
    key: "8",
    label: "setup and register directors address in UK, Rs 15,000",
  },
  {
    key: "9",
    label: "setup and register office address in UK,Rs 15,000",
  },
  {
    key: "10",
    label: "setup and register correspondence address in UK,  Rs 15,000",
  },
  {
    key: "11",
    label:
      "after receiving original documents of personal address, office address and correspondence address Rs 10,0000",
  },
  {
    key: "12",
    label:
      "receiving of  original business registration certificate, articles and seals , etc Rs 10,000",
  },
];
export default CHECK_FIELDS;
