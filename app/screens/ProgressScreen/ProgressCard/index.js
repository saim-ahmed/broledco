import React, { useState } from "react";
import { View, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import * as Animatable from "react-native-animatable";
import Text from "components/Text";

import style from "./style";

function ProgressCard({ navigation, title }) {
  const [showDetail, setShowDetail] = useState(false);
  return (
    <React.Fragment>
      <TouchableOpacity
        onPress={() => setShowDetail(!showDetail)}
        style={style.container}
      >
        <Text style={style.title}>{title}</Text>
        {showDetail ? (
          <Icon name="chevron-up" size={22} color="white" />
        ) : (
          <Icon name="chevron-down" size={22} color="white" />
        )}
      </TouchableOpacity>
      {showDetail ? (
        <Animatable.View
          useNativeDriver
          animation="fadeIn"
          easing="ease-in-out"
          duration={200}
          style={style.itemDetail}
        />
      ) : null}
    </React.Fragment>
  );
}
ProgressCard.propTypes = {
  navigation: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export default React.memo(ProgressCard);
