import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: Colors.corporate1Color,
    padding: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    marginTop: Dimensions.space2x,
    ...elevation(1),
  },
  title: {
    flex: 0.8,
    fontSize: 18,
    fontWeight: "bold",
    color: Colors.white,
  },
  itemDetail: {
    height: 100,
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius2x,
    ...elevation(2),
  },
});

export default style;
