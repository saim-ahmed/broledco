import LoginScreen from "screens/LoginScreen/loadable";
import SignUpScreen from "screens/SignUpScreen/loadable";
import MainScreen from "screens/ MainScreen/loadable";
import HomeScreen from "screens/HomeScreen/loadable";
import PaymentScreen from "screens/PaymentScreen/loadable";
import ContactScreen from "screens/ContactScreen/loadable";
import MoreTabScreen from "screens/MoreTabScreen/loadable";
import ProgressScreen from "screens/ProgressScreen/loadable";
import DirectorDetailScreen from "screens/DirectorDetailScreen/loadable";
import { createStackNavigator, createAppContainer } from "react-navigation";

const Navigator = createStackNavigator(
  {
    login: {
      screen: LoginScreen,
    },
    signUp: {
      screen: SignUpScreen,
    },
    main: {
      screen: MainScreen,
    },
    home: {
      screen: HomeScreen,
    },
    payment: {
      screen: PaymentScreen,
    },
    contact: {
      screen: ContactScreen,
    },
    more: {
      screen: MoreTabScreen,
    },
    progress: {
      screen: ProgressScreen,
    },
    directorDetail: {
      screen: DirectorDetailScreen,
    },
  },
  {
    initialRouteName: "directorDetail",
    defaultNavigationOptions: {
      headerStyle: {
        display: "none",
      },
    },
  }
);

export default createAppContainer(Navigator);
