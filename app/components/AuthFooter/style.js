import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
// import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    justifyContent: "space-around",
    alignItems: "center",
    position: "absolute",
    bottom: 0,
    width: Dimensions.screenWidth,
    backgroundColor: Colors.white,
    flexDirection: "row",
  },
  separator: {
    borderWidth: 1,
    width: 1,
    height: 30,
    backgroundColor: Colors.primary1Color,
    opacity: 0.5,
  },
  socialItem: {
    borderTopWidth: 1,
    borderTopColor: Colors.primary1Color,
    backgroundColor: Colors.white,
    flexDirection: "row",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  facebookImage: {
    width: 30,
    height: 30,
  },
  googleImage: {
    width: 35,
    height: 35,
  },
  text: {
    fontSize: 16,
    fontWeight: "bold",
    // fontFamily: "Quicksand",
    color: Colors.textGrey,
    marginLeft: Dimensions.space2x,
  },
});

export default style;
