import React from "react";
import { View, Text } from "react-native";
import Image from "components/Image";
import style from "./style";
function AuthFooter() {
  return (
    <View style={style.container}>
      <View style={style.socialItem}>
        <Image style={style.facebookImage} title="facebookIcon" />
        <Text style={style.text}>Facebook</Text>
      </View>
      <View style={style.separator} />
      <View style={style.socialItem}>
        <Image style={style.googleImage} title="googleIcon" />
        <Text style={style.text}>Google</Text>
      </View>
    </View>
  );
}
export default React.memo(AuthFooter);
