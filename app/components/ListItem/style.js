import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  scrollView: {
    height: Dimensions.screenHeight - 300,
    padding: Dimensions.space2x,
  },
  contentContainerStyle: {
    paddingBottom: Dimensions.space4x,
  },
  container: {
    justifyContent: "center",
    //   alignItems:'center',
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius3x,
    padding: Dimensions.space2x,
    marginVertical: Dimensions.space1x,
    ...elevation(1),
  },
  label: {
    fontSize: 20,
    fontWeight: "500",
    color: Colors.textBlack,
  },
});

export default style;
