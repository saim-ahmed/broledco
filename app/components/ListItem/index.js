import React from "react";
import { View, ScrollView ,TouchableOpacity} from "react-native";
import PropTypes from "prop-types";
import * as Animatable from "react-native-animatable";
import Text from "components/Text";

import style from "./style";

function ListItem({ items, type, navigation ,onPress}) {
  return (
    <ScrollView
      style={style.scrollView}
      contentContainerStyle={style.contentContainerStyle}
      bounces
      bouncesZoom
    >
      {Array.from({ length: items }, (item, index) => (
        <TouchableOpacity onPress={onPress}>
        <Animatable.View
          useNativeDriver
          animation="bounceIn"
          easing="ease-in-out"
          duration={100}
          style={style.container}
          key={item}
        >
        
          <Text style={style.label}>
            {index + 1} - Director {type}
          </Text>
          
        </Animatable.View>
        </TouchableOpacity>
      ))}
    </ScrollView>
  );
}
ListItem.propTypes = {
  navigation: PropTypes.func.isRequired,
  items: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default React.memo(ListItem);
