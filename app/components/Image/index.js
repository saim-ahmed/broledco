/**
 *
 * Image
 *
 */
import get from "lodash/get";
import React from "react";
import PropTypes from "prop-types";
import { ImageBackground } from "react-native";
import FastImage from "react-native-fast-image";

import Images from "images";

function Img({ title, uri, background, ...props }) {
  let source = {};
  if (uri) {
    source = { uri };
  } else {
    source = get(Images, title);
  }
  let Comp = FastImage;
  if (background) {
    Comp = ImageBackground;
  }
  return (
    <Comp
      resizeMethod="resize"
      source={source}
      resizeMode="contain"
      {...props}
    />
  );
}

Img.propTypes = {
  local: PropTypes.bool,
  title: PropTypes.string,
  uri: PropTypes.string,
  background: PropTypes.bool,
};

Img.defaultProps = {
  title: "logoThumb",
  uri: "",
};

export default Img;
