import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
// import Dimensions from "theme/Dimensions";

const style = StyleSheet.create({
  defaultStyle: {
    // fontFamily: "Quicksand",
    backgroundColor: Colors.transparent,
  },
});

export default style;
