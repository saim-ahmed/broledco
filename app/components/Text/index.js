import React from "react";
import { Text } from "react-native";
import PropTypes from "prop-types";
import style from "./style";

function TextComponent({ style: componentStyle, ...props }) {
  return <Text style={[style.defaultStyle, componentStyle]} {...props} />;
}
TextComponent.propTypes = {
  style: PropTypes.object,
};

TextComponent.defaultProps = {
  style: null,
};

export default React.memo(TextComponent);
