import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  container: {
    padding: Dimensions.space1x,
    backgroundColor: Colors.primary1Color,
    flexDirection: "row",
    ...elevation(2),
  },
  backButton: {
    width: 44,
    height: 44,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Colors.primary1Color,
    borderRadius: 22,
    marginLeft: 2,
    ...elevation(1),
  },
  backButtonDark: {
    backgroundColor: Colors.transparent,
  },
  backButtonIcon: {
    fontSize: 24,
    color: Colors.textBlack,
  },
  backButtonIconWhite: {
    color: Colors.white,
  },
  title: {
    fontSize: 24,
    fontWeight: "600",
    marginLeft: Dimensions.space1x,
    color: Colors.textBlack,
  },
});

export default style;
