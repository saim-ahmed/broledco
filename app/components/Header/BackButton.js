/**
 *
 * HeaderBackButton
 *
 */
import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import style from "./style";

// eslint-disable-next-line react/prefer-stateless-function
function HeaderBackButton(props) {
  const handleBackPress = () => {
    if (props.navigation) {
      props.navigation.goBack();
      return;
    }
    props.onPress();
  };

  if (!props.navigation && !props.onPress) {
    return null;
  }
  return (
    <TouchableOpacity
      style={[style.backButton, props.dark && style.backButtonDark]}
      onPress={() => handleBackPress()}
    >
      <Icon
        name="arrow-left"
        style={[style.backButtonIcon, props.dark && style.backButtonIconWhite]}
      />
    </TouchableOpacity>
  );
}

HeaderBackButton.propTypes = {
  navigation: PropTypes.object,
  onPress: PropTypes.func,
  dark: PropTypes.bool,
};

HeaderBackButton.defaultProps = {
  onPress: null,
  navigation: null,
  dark: false,
};

export default HeaderBackButton;
