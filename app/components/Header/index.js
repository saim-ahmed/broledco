import React from "react";
import PropTypes from "prop-types";
import { View } from "react-native";
import Text from "components/Text";
import BackButton from "./BackButton";
import style from "./style";
function Header({ navigation, title }) {
  return (
    <View style={style.container}>
      <BackButton navigation={navigation} />
      <Text style={style.title}>{title}</Text>
    </View>
  );
}

Header.propTypes = {
  navigation: PropTypes.func.isRequired,
  title: PropTypes.string,
};

Header.default = {
  title: "",
};

export default Header;
