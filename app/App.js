/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";

import Navigator from "./Navigator";
function App() {
  return <Navigator />;
}

export default App;
