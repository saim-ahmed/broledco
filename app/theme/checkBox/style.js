import { StyleSheet } from "react-native";

import Dimensions from "theme/Dimensions";
import Colors from "theme/Colors";

export default StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingVertical: Dimensions.space1x,
    paddingHorizontal: Dimensions.space2x,

    alignItems: "center",
    opacity: 1,
  },
  square: {
    width: 20,
    height: 20,
    borderRadius: Dimensions.radius2x,
    borderWidth: 1,
    borderColor: Colors.textBlack,
    alignItems: "center",
    justifyContent: "center",
  },
  innerSquare: {
    borderRadius: Dimensions.radius2x,
    width: 14,
    height: 14,
    backgroundColor: Colors.corporate1Color,
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    fontSize: 14,
    color: Colors.textBlack,
    marginLeft: Dimensions.space1x,
  },
  activeLabel: {
    color: Colors.corporate1Color,
  },
});
