/**
 *
 * Checkbox
 *
 */

import React, { useEffect } from "react";
import { Animated, View, TouchableOpacity, Text } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";

import Colors from "theme/Colors";

import style from "./style";

function Checkbox({ active, onPress, label }) {
  const animation = new Animated.Value(active ? 1 : 0);

  useEffect(() => {
    runAnimation(active);
  }, [active]);

  const runAnimation = isActive => {
    Animated.timing(animation, {
      toValue: isActive ? 1 : 0,
      duration: 400,
    }).start();
  };

  return (
    <TouchableOpacity onPress={onPress} style={style.container}>
      <Animated.View
        style={[
          style.square,
          {
            borderColor: animation.interpolate({
              inputRange: [0, 1],
              outputRange: [Colors.textBlack, Colors.corporate3Color],
            }),
          },
        ]}
      >
        <Animated.View
          style={[
            style.innerSquare,
            {
              transform: [
                {
                  scale: animation,
                },
              ],
              opacity: animation,
            },
          ]}
        >
          <Icon name="check" size={12} color="white" />
        </Animated.View>
      </Animated.View>
      {label && (
        <View style={style.labelContainer}>
          <Text style={[style.label, active && style.activeLabel]}>
            {label}
          </Text>
        </View>
      )}
    </TouchableOpacity>
  );
}

Checkbox.propTypes = {
  active: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
  label: PropTypes.string.isRequired,
};

Checkbox.defaultProps = {
  onPress: () => null,
};

export default React.memo(Checkbox);
