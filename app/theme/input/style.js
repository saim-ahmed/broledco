import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const INPUT_HEIGHT = 30;
const MULTILINE_HEIGHT = 130;

const style = StyleSheet.create({
  container: {
    marginVertical: Dimensions.space2x,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    padding: Dimensions.space1x,
  },
  textHolder: {
    // position: "relative",
    // borderWidth: 1,
    // borderColor: Colors.primary1Color,
    flexDirection: "row",
    height: INPUT_HEIGHT,
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  multilineContainer: {
    height: MULTILINE_HEIGHT,
  },
  input: {
    minHeight: INPUT_HEIGHT,
    // width: Dimensions.screenWidth - 20,
    flex: 1,
    marginHorizontal: Dimensions.space2x,
    padding: Dimensions.space1x,
    // paddingHorizontal: Dimensions.space2x,
    borderRadius: Dimensions.radius2x,
    borderBottomWidth: 2,
    color: Colors.primary3Color,
    backgroundColor: Colors.white,
    alignSelf: "center",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    textAlignVertical: "center",
    position: "relative",
    // fontFamily: "Quicksand",
    borderBottomColor: Colors.corporate1Color,
  },
  labeledInput: {
    minHeight: INPUT_HEIGHT + 10,
    borderWidth: 1,
    ...elevation(1),
  },
  label: {
    marginBottom: Dimensions.space1x,
    marginHorizontal: Dimensions.space2x,
    fontWeight: "500",
    fontSize: 16,
  },
  blurStyle: {
    borderColor: Colors.primary2Color,
  },
  focusStyle: {
    borderColor: Colors.corporate1Color,
  },
  multiline: {
    height: MULTILINE_HEIGHT,
    padding: Dimensions.space2x,
    textAlignVertical: "top",
  },
  placeholderContainer: {
    position: "absolute",
    left: Dimensions.space3x,
    height: INPUT_HEIGHT,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  placeholder: {
    color: Colors.textGrey,
    fontSize: 16,
    width: Dimensions.screenWidth - Dimensions.space12x,
  },
  labeledPlaceHolder: {
    color: Colors.primary2Color,
  },
  loaderContainer: {
    position: "absolute",
    right: Dimensions.space1x,
    height: 44,
    top: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  error: {
    backgroundColor: Colors.corporate1Color,
    paddingHorizontal: Dimensions.space2x,
    paddingVertical: Dimensions.space1x / 3,
    borderRadius: Dimensions.radius4x,
    // color: Colors.errorForeground,
    position: "absolute",
    bottom: -Dimensions.space3x,
    right: Dimensions.space4x,
    // fontWeight: "600",
    // fontSize: 13,
    overflow: "hidden",
  },
  // error: {
  //   marginHorizontal: Dimensions.space2x + 2,
  //   marginTop: -Dimensions.space1x,
  //   borderRadius: Dimensions.radius1x,
  //   backgroundColor: Colors.corporate1Color
  // },
  errorLabel: {
    color: Colors.white,
    fontWeight: "600",
    fontSize: 13,
  },
  validIcon: {
    position: "absolute",
    right: Dimensions.space2x,
  },
});

export default style;
