import React from "react";
import { View, Text, TextInput, Animated } from "react-native";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import Colors from "theme/Colors";
import style from "./style";

class Input extends React.PureComponent {
  state = {
    focused: false,
  };

  animation = new Animated.Value(this.props.isValid ? 1 : 0);

  componentWillReceiveProps(nextProps) {
    this.runAnimation(nextProps.isValid);
  }

  runAnimation(isValid) {
    Animated.timing(this.animation, {
      toValue: isValid ? 1 : 0,
      duration: 400,
    }).start();
  }

  defaultSettings = {
    multiline: false,
    clearButtonMode: "unless-editing",
  };

  focus() {
    if (!this.input) {
      return;
    }
    this.input.focus();
  }

  render() {
    const { label, multiline, onBlur, onFocus, error, innerRef } = this.props;
    return (
      <View style={style.container}>
        {label ? <Text style={style.label}>{label}</Text> : null}
        <View
          style={[
            style.textHolder,
            multiline ? style.multilineContainer : style.singlelineContainer,
          ]}
        >
          <TextInput
            ref={node => {
              this.input = node;
              innerRef(node);
            }}
            underlineColorAndroid={Colors.transparent}
            placeholderTextColor={Colors.placeholderColor}
            onFocus={() => {
              this.setState({ focused: true });
              onFocus();
            }}
            onBlur={() => {
              this.setState({ focused: false });
              onBlur();
            }}
            style={[
              this.state.focused ? style.focusStyle : style.blurStyle,
              style.input,
              label ? style.labeledInput : null,
              multiline ? style.multiline : {},
            ]}
            {...this.defaultSettings}
            {...this.props}
          />
          <Animated.View
            style={[
              style.validIcon,
              {
                opacity: this.animation,
              },
            ]}
          >
            <Icon name="check" size={18} color={Colors.corporate1Color} />
          </Animated.View>
        </View>

        {error ? (
          <View style={style.error}>
            <Text style={style.errorLabel}>{error}</Text>
          </View>
        ) : null}
      </View>
    );
  }
}

Input.propTypes = {
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  placeholder: PropTypes.any,
  error: PropTypes.func,
  label: PropTypes.string,
  multiline: PropTypes.bool,
  isValid: PropTypes.bool,
  innerRef: PropTypes.func,
};

Input.defaultProps = {
  onFocus: () => null,
  onBlur: () => null,
  innerRef: () => null,
  placeholder: null,
  error: null,
  multiline: false,
  isValid: false,
};

export default React.memo(Input);
