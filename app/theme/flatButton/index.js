/**
 *
 * FlatButton
 *
 */

import React from "react";
import PropTypes from "prop-types";
import Icon from "react-native-vector-icons/Feather";
import { TouchableOpacity, Text } from "react-native";
import style from "./style";
function FlatButton(props) {
  return (
    <TouchableOpacity
      style={[
        style.button,
        props.round ? style.roundButton : null,
        props.flex ? style.flex : {},
        props.secondary ? style.secondaryButton : {},
        props.active ? style.activeButton : {},
        props.disabled ? style.disabled : style.enabled,
        props.elevated ? style.elevated : null,
      ]}
      onPress={props.disabled ? () => null : props.onPress}
    >
      {props.iconName ? (
        <Icon
          style={[
            style.icon,
            props.secondary ? style.secondaryIcon : {},
            props.active ? style.activeIcon : {},
          ]}
          name={props.iconName}
        />
      ) : null}

      <Text
        style={[
          style.label,
          props.secondary ? style.secondaryLabel : {},
          props.active ? style.activeLabel : {},
        ]}
      >
        {props.label}
      </Text>
    </TouchableOpacity>
  );
}

FlatButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  iconName: PropTypes.string,
  secondary: PropTypes.bool,
  disabled: PropTypes.bool,
  flex: PropTypes.bool,
  elevated: PropTypes.bool,
  active: PropTypes.bool,
  round: PropTypes.bool,
};

FlatButton.defaultProps = {
  secondary: false,
  disabled: false,
  elevated: true,
  iconName: "",
  flex: false,
  active: false,
};
export default FlatButton;
