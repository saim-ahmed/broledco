import { StyleSheet } from "react-native";
import Colors from "theme/Colors";
import Dimensions from "theme/Dimensions";
import elevation from "theme/elevation";

const style = StyleSheet.create({
  button: {
    backgroundColor: Colors.white,
    borderRadius: Dimensions.radius3x,
    margin: Dimensions.space1x,
    padding: Dimensions.space2x,
    paddingVertical: Dimensions.space1x,
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    height: 44,
    ...elevation(1),
  },
  roundButton: {
    borderRadius: 20,
  },
  secondaryButton: {
    backgroundColor: Colors.corporate1Color,
  },
  activeButton: {
    borderWidth: 1,
    borderColor: Colors.corporate1Color,
  },
  flex: {
    flex: 1,
  },
  label: {
    fontSize: 18,
    color: Colors.primary3Color,
    maxWidth: Dimensions.screenWidth - Dimensions.space6x - 40,
    fontWeight: "800",
  },
  secondaryLabel: {
    color: Colors.primary1Color,
  },
  activeLabel: {
    color: Colors.corporate1Color,
  },
  disabled: {
    opacity: 0.5,
    ...elevation(0),
  },
  enabled: {
    opacity: 1,
  },
  elevated: {
    ...elevation(2),
  },
  icon: {
    fontSize: 22,
    marginRight: Dimensions.space2x,
    color: Colors.black,
  },
  secondaryIcon: {
    color: Colors.primary1Color,
  },
  activeIcon: {
    color: Colors.corporate1Color,
  },
});

export default style;
